<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table ='kategori';
    protected $fillable =['judul', 'edisi'];
    public function dafta(){
    	return $this->belongsTo('App\dafta');
    }
}
