<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dafta extends Model
{
    protected $table ='daftar';
    protected $fillable =['nama', 'kode', 'tahun'];

    public function tags(){
    	return $this->hasMany('App\Tag');
    }
}
