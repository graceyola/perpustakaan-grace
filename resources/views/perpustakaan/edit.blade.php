@extends('layout/main');

@section('title', 'Form ')


@section('container')
    <div class="container">
        <div class="row">
            <div class="col-10">
                <h1 class="mt-2">Form ubah Data Perpustakaan</h1>
                @foreach($daftar as $dft)
                    <form method="post" action="/perpustakaan/index">
                    {{csrf_field()}}
                    
                    
                    @csrf
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" required="required" class="form-control" id="nama" value="{{$dft->nama}}" placeholder="Masukkan Nama Buku" name="nama">
                        </div>
                        <div class="form-group">
                            <label for="kode">Kode</label>
                            <input type="text" required="required" class="form-control" id="kode" value="{{$dft->kode}}" placeholder="Masukkan Kode Buku" name="kode">
                        </div>
                        <div class="form-group">
                            <label for="tahun">Tahun</label>
                            <input type="tahun" required="required" class="form-control" id="tahun" value="{{$dft->tahun}}" placeholder="Masukkan tahun terbit Buku" name="tahun">
                        </div>
                        <button type="submit" class="btn btn-primary">Ubah Data </button>
                        
                    </form>
                @endforeach

            </div>
           
        </div>
    </div>
@endsection

    